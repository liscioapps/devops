# Tunnel Configuration for Liscio Apps

The domains `tunnel.liscioapps.com` and `tunnel-east.liscioapps.com` are useful in the development of applications that require a connection from the outside world - such as webhooks or other inbound API requests.  These tunnels are similar to a simplified `ngrok`, but one that we run and control ourselves so (a) we can control cost and (b) we can control encryption. 

## Basics
* The tunnels run with an Unbuntu NGINX server hosted on a small EC2 instance in AWS.
* This instance has a static IP address of `54.209.82.15`
* DNS for `tunnel.` and `tunnel-east.` are set to that address
* A simple SSH tunnel connects tunnels traffic between a port on the remote EC2 instance and your local developer laptop
* When a user hits the URL, NGINX does a `proxy pass` of all of the user's requests directly onto the developer's laptop through the tunnel
* ?
* Profit

## Ports
As you can see from the NGINX config files in this folder, the following ports correspond to their respective domains:

| Domain | Port |
| ------ | ------ |
| tunnel.liscioapps.com | 3333 |
| tunnel-east.liscioapps.com | 4444 |

## Connections
So to connect, you'll need the private key file `liscio-pair.pem`.  Let's assume you put that at `~/.ssh/liscio-pair.pem`.  Then if you wanted to connect port 5000 on your *local* computer to `tunnel.liscioapps.com` you would run:

```
ssh -i ~/.ssh/liscio-pair.pem -N -T -R 3333:localhost:5000 ubuntu@tunnel.liscioapps.com
```

Or, to connect directly to the instance to see what is running

```
ssh -i ~/.ssh/liscio-pair.pem ubuntu@tunnel.liscioapps.com
```

## Encryption
SSH is natively encrypted in transit, so the actual SSH tunnel between EC2 and the developer's laptop is encrypted using the PEM mentioned above.

For outside traffic coming in - e.g., HTTPS encryption we use [`Let's Encyprt`](https://letsencrypt.org/).  To create the certificates initially, we used `certbot`.  Installing the NGINX plugin for certbot is helpful (`sudo apt-get install python-certbot-nginx`).  Then, issuing and configuring the certificates is just as easy as running:

```
certbot --nginx
```

Certificates are now auto-renewed by running 

```
certbot renew
```

in the file `/etc/cron.daily/letsencrypt`

## Use Cases

* [Onboarding Buddy development](https://gitlab.com/liscioapps/buddy/app/-/blob/master/dev/tunnel.sh)
