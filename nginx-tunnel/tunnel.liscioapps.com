server {
    server_name tunnel.liscioapps.com;

    access_log /var/log/nginx/$host;

    listen 80 default_server;
    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/tunnel.liscioapps.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/tunnel.liscioapps.com/privkey.pem; # managed by Certbot


    location / {
	    proxy_pass http://localhost:3333/;
	    proxy_set_header X-Real-IP $remote_addr;
	    proxy_set_header Host $host;
	    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
	    proxy_redirect off;
    }

    location ~ /.well-known {
        root /var/www/letsencrypt;
        allow all;
    }

    error_page 502 /50x.html;
    location = /50x.html {
	    root /usr/share/nginx/html;
    }

}
