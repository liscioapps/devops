# Inslets Tunnel

## Creating a new droplet
```bash
inletsctl create --provider digitalocean \
  --region lon1 \
  --access-token-file ~/.ssh/do-api
```

## Connecting to that droplet


## HTTPS using Caddy
* Install the repository
```
echo "deb [trusted=yes] https://apt.fury.io/caddy/ /" \
    | sudo tee -a /etc/apt/sources.list.d/caddy-fury.list
```
* `sudo apt update`
* `sudo apt install caddy`

## Reference
* https://github.com/inlets/inlets
* https://github.com/inlets/inletsctl
* https://blog.alexellis.io/https-inlets-local-endpoints/ 
* https://github.com/caddyserver/caddy
